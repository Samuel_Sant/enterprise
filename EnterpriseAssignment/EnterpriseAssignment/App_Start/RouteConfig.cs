﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace EnterpriseAssignment
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    name: "Category",
            //    url: "{controller}/{action}/{CategoryName}",
            //    defaults: new { controller = "Category", action = "CategoryName" }
            //);


            routes.MapRoute(
                name: "Sale",
                url: "{controller}/{action}",
                defaults: new { controller = "Category", action = "Sale" }
            );
            //Pass Action name as parameter and put into 1 route
            routes.MapRoute(
                name: "Evening",
                url: "{controller}/{action}",
                defaults: new { controller = "Category", action = "Evening" }
            );

            routes.MapRoute(
                name: "Casual",
                url: "{controller}/{action}",
                defaults: new { controller = "Category", action = "Casual" }
            );

            routes.MapRoute(
                name: "Accessories",
                url: "{controller}/{action}",
                defaults: new { controller = "Category", action = "Accessories" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
