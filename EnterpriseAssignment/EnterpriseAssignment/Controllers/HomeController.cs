﻿using Common.Models;
using DataAccess;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Threading.Tasks;
using Dropbox.Api;
using System.Text;
using Dropbox.Api.Files;

namespace EnterpriseAssignment.Controllers
{
    public class HomeController : Controller
    {
        private BackendContext db = new BackendContext();
        public ActionResult Index()
        {
            List<Item> items = db.Items.OrderByDescending(d => d.DateAdded).Take(5).ToList();
            return View(items);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public async Task<ActionResult> GetImage()
        {
            var imageName = "anime.png";
            DropboxClient dropboxClient = new DropboxClient("oRS4_FHaJcAAAAAAAAAAGVoDia3CscgQ8hAlPob-iCNmS0IO8SU-1CUW6k2ABTUQ"); 
            try
            {
                using (var response = await dropboxClient.Files.DownloadAsync("/" + imageName))
                {
                    var imageRetrieved = response.GetContentAsByteArrayAsync();
                    imageRetrieved.Wait();
                    var imageHold = imageRetrieved.Result;
                    string storagePath = Path.Combine(Server.MapPath("~/Download"), imageName);
                    System.IO.File.WriteAllBytes(storagePath, imageHold); 
                    return RedirectToAction("Index");
                }
            }
            catch (ApiException<DownloadError> e)
            {
                ModelState.AddModelError("", e.ToString());
                return RedirectToAction("Index");
            }
        }

    }
}