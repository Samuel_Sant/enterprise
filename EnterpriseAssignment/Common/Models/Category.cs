﻿namespace Common.Models
{
    public class Category
    {
        public static Category Accessories =>
            new Category
            {
                Title = "Accessories",
                Link = @"..\Category\Accessories",
                ImageUrl = "https://i.etsystatic.com/9623089/d/il/9f8026/1761926714/il_340x270.1761926714_m8qp.jpg?version=0",
                Description = "The Accessory Category!"
            };

        public static Category Casual =>
            new Category
            {
                Title = "Casual",
                Link = @"..\Category\Casual",
                ImageUrl = "https://images-na.ssl-images-amazon.com/images/I/71xOqWS018L._UX679_.jpg",
                Description = "The Casual Category!"
            };

        public static Category Evening =>
            new Category
            {
                Title = "Evening",
                Link = @"..\Category\Evening",
                ImageUrl = "http://cdn.shopify.com/s/files/1/2995/0036/products/sewa.14_1200x1200.jpg?v=1520319392",
                Description = "The Evening Category!"
            };

        public static Category Sale =>
            new Category
            {
                Title = "Sale",
                Link = @"..\Category\Sale",
                ImageUrl = "https://images-na.ssl-images-amazon.com/images/I/51td1Yvc4aL._UX522_.jpg",
                Description = "The Sale Category!"
            };

        public string Title { get; set; }

        public string Link { get; set; }

        public string ImageUrl { get; set; }

        public string Description { get; set; }
    }
}
