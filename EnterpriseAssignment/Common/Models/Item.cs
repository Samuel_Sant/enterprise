﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Models
{
    public enum ClothingCategory
    {
        Casual = 1,
        Evening = 2,
        Accessories = 3,
        Sale = 4
    }

    public class Item
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ItemID { get; set; }

        [Required]
        [StringLength(50)]
        public string ItemName { get; set; }

        [Required]
        public double ItemPrice { get; set; }

        [Required]
        public string ItemDescription { get; set; }

        [Required]
        public ClothingCategory ItemType { get; set; }

        [Required]
        public string ItemPicture { get; set; }

        [Required]
        public DateTime DateAdded { get; set; }

        public string CreatedBy { get; set; }
    }
}